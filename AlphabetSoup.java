/* Priyanka Kishore, 01/19/20
 * Enlighten IT Consulting
 * Coding Challenge for Summer 2020 Internship
 * "ALPHABET SOUP" */

import java.io.FileNotFoundException;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.util.ArrayList;
import java.util.Scanner;
import java.io.File;

public class AlphabetSoup {

    /* Directions in which a word can be searched in the grid */
    public enum Direction {
        UPLEFT(-1,-1), UP(-1,0), UPRIGHT(-1,1), RIGHT(0,1), 
        DOWNRIGHT(1,1), DOWN(1,0), DOWNLEFT(1,-1), LEFT(0,-1);

        private final int deltaRow, deltaCol;

        Direction (int deltaRow, int deltaCol) {
            this.deltaRow = deltaRow;
            this.deltaCol = deltaCol;
        }

        public int getDeltaRow() {
            return this.deltaRow;
        }

        public int getDeltaCol() {
            return this.deltaCol;
        }
    }

    private static char[][] grid;
    private static int numRows, numCols;
    private static ArrayList<String> wordsToFind;
    private static String startCoord, endCoord;

    public static void main(String[] args) throws FileNotFoundException {

        // user input of file name
        System.out.print("Enter name of ASCII text file containing word search:\n>> ");
        Scanner sc = new Scanner(System.in);
        String fileName = sc.nextLine();
       
        try {
            Scanner scanner = new Scanner(new File(fileName));

            // reading and parsing contents of input file
            readAndParseFile(scanner);

            // searching for each word in grid, output solution
            System.out.println("[ANSWER KEY]");
            for (int i = 0; i < wordsToFind.size(); i++) {
                String currWord = wordsToFind.get(i);
                String answer = search(currWord);
                System.out.println(answer);
            }
        } catch (FileNotFoundException e) {
            System.out.println(e);
        }
    }

    /* Reads input file data and parses each line into corresponding data structure */
    private static void readAndParseFile(Scanner scanner) {
        ArrayList<String> inputLines = new ArrayList<>();
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            inputLines.add(line);
        }

        parseGridSize(inputLines.get(0));
        parseGrid(inputLines);
        parseWordsToFind(inputLines);
    }

    /* Parses first line of `inputLines` using REGEX to store size of grid */
    private static void parseGridSize(String firstLine) {
        Pattern p = Pattern.compile("^(?<row>\\d+)x(?<col>\\d+)$");
        Matcher m = p.matcher(firstLine);
        while (m.find()) {
            numRows = Integer.parseInt(m.group("row"));
            numCols = Integer.parseInt(m.group("col"));
        }
    }

    /* Parses next `numRows` lines of `inputLines` to create grid */
    private static void parseGrid(ArrayList<String> inputLines) {
        grid = new char[numRows][numCols];

        for (int i = 0; i < numRows; i++) {
            String currRow = inputLines.get(i + 1);
            int colIndex = 0;
            for (int j = 0; j < currRow.length(); j += 2) {
                grid[i][colIndex++] = currRow.charAt(j);
            }            
        }
    }

    /* Parses words from `inputLines` to be found on word search grid */
    private static void parseWordsToFind(ArrayList<String> inputLines) {
        wordsToFind = new ArrayList<>();
        for (int i = numRows + 1; i < inputLines.size(); i++) {
            String currLine = inputLines.get(i);
            currLine = currLine.replaceAll("\\s+", ""); // delete white space
            wordsToFind.add(currLine);
        }
    }

    /* Searches for current `word` on the word search grid */
    private static String search(String word) {
        int charIndex = 0;

        // check each cell in grid to find 1st char of `word`
        for (int i = 0; i < numRows; i++) { 
            for (int j = 0; j < numCols; j++) {
                if (grid[i][j] == word.charAt(charIndex) && explore(i, j, word, charIndex + 1, null)) {
                    return word + " " + startCoord + " " + endCoord; // found word!
                } // if false, keep cell searching
            }
        }
        return "ERROR: Word \"" + word + "\" does not exist in word search grid.";
    }

    private static boolean explore(int row, int col, String word, int charIndex, Direction currDirection) {
        // `word` is FOUND
        if (charIndex >= word.length()) {
            return true; 
        }

        // Search not started (direction to search in not yet determined)
        if (currDirection == null) { 
            startCoord = row + ":" + col;

            // look in all directions for 2nd char
            for (Direction dir : Direction.values()) { 
                int newRow = row + dir.getDeltaRow(),
                    newCol = col + dir.getDeltaCol();

                // if found 2nd character:
                if (charIndex < word.length() && stillInGrid(newRow, newCol) && grid[newRow][newCol] == word.charAt(charIndex)) {
                    if (explore(newRow, newCol, word, charIndex + 1, dir) == false) { 
                        continue; // wrong direction, explore other directions
                    }
                    return true; // found entire word!
                } 
            }
        
        // Search has started (direction to search in is determined), search for remaining characters
        } else {
            int newRow = row + currDirection.getDeltaRow(), 
                newCol = col + currDirection.getDeltaCol();

            // if found 3rd+ char
            if (charIndex < word.length() && stillInGrid(newRow, newCol) && grid[newRow][newCol] == word.charAt(charIndex)) {
                endCoord = newRow + ":" + newCol;
                return explore(newRow, newCol, word, charIndex + 1, currDirection);
            }
        }

        return false; // `word` does not exist in `grid`
    }

    private static boolean stillInGrid (int row, int col) {
        return row >= 0 && row < numRows && col >= 0 && col < numCols;
    }
}